import React from 'react';
import { Link } from "react-router-dom";
import {Menu} from 'element-react';

class AppHeader extends React.Component{

    render() {
        return (
            <header className="header">
             <Menu theme="dark" className="el-menu-demo" mode="horizontal">
             	<Menu.Item index="1">
             		<Link to="/">Home</Link>
             	</Menu.Item>
             	<Menu.Item index="2">
             		<Link to="/about">About</Link>
             	</Menu.Item>
             </Menu>
            </header>
        );
    }
}

export default AppHeader;