import React from 'react';
import axios from 'axios';
import {Input} from 'element-react';

class HomePage extends React.Component{

	constructor(props) {
        super(props);

        this.state = {
        	nbuCurrencies: [],
        	filteredNbuCurrencies: [],
			searchValue: ''
        };
    }

    componentWillMount(){
        this.doRequest();
    }

    doRequest(){
    	axios.get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
		  .then(response => {

		    this.setState({nbuCurrencies: response.data});
            this.copyNbuCurrencies();

		  })
		  .catch(error => {
		    console.log(error);
		  });
    }

    copyNbuCurrencies() {
        this.setState({filteredNbuCurrencies: this.state.nbuCurrencies.slice()});
    }
    search(event){
        console.log(event);
    }
    render() {
        return (
            <div className='HomePage'>

                <table>
                    <thead className="ui-table-thead">
                    <tr>
                        <th colSpan="2">
                            <div className="form-el">
                                        <Input placeholder="Please input" onChange={this.search.bind(this)} />
                                      </div>
                                    </th>
                                    <th>Курс</th>
                                  </tr>
                                  </thead>
                                  <tbody className=" ui-table-tbody">
                                      {this.state.filteredNbuCurrencies.map((currency, index) => {
                                          return (<tr key={currency.cc}>
                                              <td className="nbu_id">{currency.cc}</td>
                                              <td className="cryptoex__title-td">
                                                  <span className="nbu__title">{currency.txt}</span>
                                              </td>
                                              <td className="cryptoex__rate">
                                                  <span>{currency.rate}</span>
                                              </td>
                                          </tr>)
                                      })}

                                  </tbody>
                                </table>

                </div>
        );
    }
}

export default HomePage;