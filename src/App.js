import React, { Component } from 'react';
import RouterOutlet from './router/RouterOutlet';

import AppHeader from './components/common/AppHeader';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <AppHeader/>
          <RouterOutlet/>
      </div>
    );
  }
}

export default App;
